const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth.js")

//Create course
router.post("/", auth.verify, (req,res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});

//Retrieve all courses
router.get("/all", (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

/*
	Mini-Activity:
	1. Create a route that will retrieve all Active courses
	2. No need for user to login
	3. Create a controller that will return ALL Active courses
	4. Send your Postman output screenshot in our batch hangouts
*/

//Retrieve active courses
router.get("/", (req,res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController));
})

//Retrieve specific courses
router.get("/:courseId", (req,res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

//Update a course
router.put("/:courseId", auth.verify, (req,res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

//Archiving a course
router.put("/:courseId/archive", auth.verify, (req,res) => {

	const data = auth.decode(req.headers.authorization);;
	if(data.isAdmin) {
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}else {
		res.send("User is not an Admin")
	}
	

});

//export the router object for index.js file
module.exports = router;



